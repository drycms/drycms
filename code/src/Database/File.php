<?php
namespace Database;

class File extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_FILE);
    }

    public function getCategoryCount()
    {
        $sql = $this->getSql();
        $sql->field('dry_category,count(*) as total');
        $sql->table($this->getTable());
        $sql->groupBy('', 'dry_category');
        $list = $this->fetchAll($sql->get());
        return $list;
    }

    public function getListByCategory($page, $limit, $extension = [])
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->where('dry_category', '=', $extension['categoryId']);
        $sql->setOrder('id', 'desc');
        $sql->setPage($page);
        $sql->setPageSize($limit);
        $sql->setLimit();
        $list = $this->fetchAll($sql->get());
        return $list;
    }

}