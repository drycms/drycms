<?php
namespace CommonTrait;

trait Template
{

    private $template = ['index' => 'index', 'show' => 'show', 'new' => 'new', 'edit' => 'edit', 'login' => 'login'];

    private $twigFile = '';

    private $twigCommonData = [];

    private $twigData = [];

    private $dir = '';

    private $nameSpace = '';

    public function setNameSpace($nameSpace)
    {
        $this->nameSpace = $nameSpace;
        return $this;
    }

    public function getNameSpace()
    {
        return $this->nameSpace;
    }

    public function setDir($dir)
    {
        $this->dir = $dir;
        return $this;
    }

    public function getDir()
    {
        return $this->dir;
    }

    public function setFile($file)
    {
        $this->twigFile = $file;
    }

    public function setCommonData($data)
    {
        $this->twigCommonData = $data;
    }

    public function setData($data)
    {
        $this->twigData = $data;
    }

    public function show()
    {
        $data = array_merge($this->twigCommonData, $this->twigData);
        $twig = $this->getInstance('twig');
        $content = $twig->render($this->twigFile, $data);
        $this->response->end($content);
    }

    public function setTemplate($name, $file)
    {
        $this->template[$name] = $file;
    }

    public function getTemplate($name)
    {
        $dir = $this->getDir();
        $file = $this->template[$name];
        $nameSpace = $this->getNameSpace();
        return "@{$nameSpace}/{$dir}/{$file}.html.twig";
    }

    public function getTemplateIndex()
    {
        return $this->getTemplate('index');
    }

    public function getTemplateShow()
    {
        return $this->getTemplate('show');
    }

    public function getTemplateNew()
    {
        return $this->getTemplate('new');
    }

    public function getTemplateEdit()
    {
        return $this->getTemplate('edit');
    }

    public function getTemplateLogin()
    {
        return $this->getTemplate('login');
    }

}