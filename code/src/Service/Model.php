<?php
namespace Service;

class Model extends Base
{

    public function getDatabase()
    {
        return $this->get(DATABASE_MODEL);
    }

    public function getFieldList($id)
    {
        $model = $this->one($id);
        $fields = $this->get(SERVICE_FIELD)->more($model->dry_field);
        $typeIn = array2stringByKey($fields, 'dry_field_type');
        $types = $this->get(SERVICE_FIELDTYPE)->more($typeIn);
        $typeMap = array2mapObject($types, 'id');
        foreach($fields as $k => $v){
            $v->dry_field_type_data = $typeMap[$v->dry_field_type];
            $fields[$k] = $v;
        }
        return $fields;
    }

    public function getModelByAlias($alias = '')
    {
        $database = $this->getDatabase();
        return $database->getModelByAlias($alias);
    }

    public function getFieldListByModel($model = null)
    {
        $fields = $this->get(SERVICE_FIELD)->more($model->dry_field);
        $fieldMap = array2mapObject($fields, 'id');
        $list = $this->get(SERVICE_MODELFIELD)->getListByModelId($model->id);
        foreach($list as $k => $v){
            $temp = $fieldMap[$v->dry_field];
            $v->dry_field_data = $temp;
            $list[$k] = $v;
        }
        return $list;
    }

    public function getSearchFieldListByModel($model = null)
    {
        $fields = $this->get(SERVICE_FIELD)->more($model->dry_field);
        $fieldMap = array2mapObject($fields, 'id');
        $list = $this->get(SERVICE_MODELFIELD)->getListByModelId($model->id);
        $result = [];
        foreach($list as $k => $v){
            if($v->dry_is_search == '1'){
                $temp = $fieldMap[$v->dry_field];
                $v->dry_field_data = $temp;
                $result[] = $v;
            }
        }
        usort($result, function($c1, $c2){
            return compareValue($c1->dry_field_search_sort, $c2->dry_field_search_sort);
        });
        return $result;
    }

    public function getAliasControllerName()
    {
        $database = $this->getDatabase();
        return $database->getAliasControllerName();
    }

}