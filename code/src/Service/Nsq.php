<?php
namespace Service;
use nsqphp\nsqphp;
use nsqphp\Message\Message;
use nsqphp\Lookup\FixedHosts;

class Nsq extends Base
{

    public function publish($list, $topic, $message)
    {
        $nsq = new nsqphp();
        $nsq->publishTo($list[0])->publish($topic, new Message($message));
    }

    public function subscribe($list, $topic, $channel, $cb)
    {
        $lookup = new FixedHosts($list);
        $nsq = new nsqphp($lookup);
        $nsq->subscribe($topic, $channel, $cb)->run();
    }

}