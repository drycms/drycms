<?php
namespace Service;

class Base
{

    use \CommonTrait\Base;

    public function getTable()
    {
        $database = $this->getDatabase();
        return $database->getTable();
    }

    public function getPdo()
    {
        $database = $this->getDatabase();
        return $database->getPdo();
    }

    public function setPdo($pdo)
    {
        $database = $this->getDatabase();
        return $database->setPdo($pdo);
    }

    public function beginTransaction()
    {
        $database = $this->getDatabase();
        return $database->beginTransaction();
    }

    public function commit()
    {
        $database = $this->getDatabase();
        return $database->commit();
    }

    public function rollBack()
    {
        $database = $this->getDatabase();
        return $database->rollBack();
    }

    public function add($data = [])
    {
        $database = $this->getDatabase();
        return $database->insert($data);
    }

    public function update($data = [], $id, $operate = '=')
    {
        $database = $this->getDatabase();
        return $database->update($data, $id, $operate);
    }

    public function updateMore($data, $id)
    {
        return $this->update($data, $id, 'in');
    }

    public function one($id)
    {
        $database = $this->getDatabase();
        return $database->one($id);
    }

    public function more($idIn)
    {
        $database = $this->getDatabase();
        return $database->more($idIn);
    }

    public function delete($idIn)
    {
        $database = $this->getDatabase();
        return $database->delete($idIn);
    }

    public function deleteByField($field = '', $value = '')
    {
        $database = $this->getDatabase();
        return $database->deleteByField($field, $value);
    }

    public function getOneByField($field, $value)
    {
        $database = $this->getDatabase();
        return $database->getOneByField($field, $value);
    }

    public function getOneByFields($data)
    {
        $database = $this->getDatabase();
        return $database->getOneByFields($data);
    }

    public function getMoreByField($field, $value)
    {
        $database = $this->getDatabase();
        return $database->getMoreByField($field, $value);
    }

    public function getMoreByFields($data)
    {
        $database = $this->getDatabase();
        return $database->getMoreByFields($data);
    }

    public function getList($idIn = '')
    {
        $database = $this->getDatabase();
        return $database->getList($idIn);
    }

    public function setDataSort($sqls = [])
    {
        $database = $this->getDatabase();
        $database->setDataSort($sqls);
    }

    public function getIndex($page, $limit, $extension = [])
    {
        $database = $this->getDatabase();
        return $database->getIndex($page, $limit, $extension);
    }

    /*
        事务
        $ext = [
            [
                'service' => '',
                'action' => SQL_INSERT|SQL_UPDATE,
                'insert' => [],
                'update' => [],
                'updateId' => 1
            ]
        ];
    */
    public function transaction($mainService, $ext = [])
    {
        $bool = false;
        if(empty($ext)){
            return $bool;
        }
        $service = $this->get($mainService);
        $pdo = $service->getPdo();
        $service->beginTransaction();
        try{
            foreach($ext as $item){
                $serviceTemp = $this->get($item['service']);
                $serviceTemp->setPdo($pdo);
                if($item['action'] == SQL_INSERT){
                    $serviceTemp->add($item['insert']);
                }
                else if($item['action'] == SQL_UPDATE){
                    $serviceTemp->update($item['update'], $item['updateId']);
                }
            }
            $service->commit();
            $bool = true;
        }
        catch(\Exception $e){
            $service->rollBack();
        }
        return $bool;
    }

}