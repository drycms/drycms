<?php
namespace Service;

class GlobalId extends Base
{

    private $cacheGlobalId = null;

    public function __construct()
    {
        $this->cacheGlobalId = $this->get(CACHE_GLOBALID);
    }

    public function getId($table)
    {
        return $this->cacheGlobalId->getId($table);
    }

}