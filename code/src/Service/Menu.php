<?php
namespace Service;

class Menu extends Base
{

    public function getDatabase()
    {
        return $this->get(DATABASE_MENU);
    }

    public function getParent()
    {
        $database = $this->getDatabase();
        return $database->getParent();
    }

}