<?php
namespace Service;

class ModelField extends Base
{

    public function getDatabase()
    {
        return $this->get(DATABASE_MODELFIELD);
    }

    public function deleteByModelId($modelId)
    {
        $database = $this->getDatabase();
        return $database->deleteByModelId($modelId);
    }

    /*
        编辑时删除不需要的字段
    */
    public function deleteByExclude($modelId, $fieldIn)
    {
        $database = $this->getDatabase();
        return $database->deleteByExclude($modelId, $fieldIn);
    }

    public function getListByModelId($modelId)
    {
        $database = $this->getDatabase();
        return $database->getListByModelId($modelId);
    }

}