<?php
namespace Core;

class Pdo
{

    private $attr = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_PERSISTENT => false,
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8mb4',
        \PDO::ATTR_ORACLE_NULLS => false,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ
    ];

    public function get($key)
    {
        $config = getConfig('mysql', $key);
        $dns = "mysql:host={$config['host']};port={$config['port']};dbname={$config['dbname']}";
        return new \PDO($dns, $config['username'], $config['password'], $this->attr);
    }

}