<?php
namespace Application\Api\Controller;

class Automate extends Base
{

    private function hasAddTimeField($fields)
    {
        $list = [];
        foreach($fields as $field){
            if($field->dry_field_data->dry_is_add_date_time){
                $list[] = $field->dry_field_data->dry_field;
            }
        }
        return $list;
    }

    private function check($data)
    {
        $alias = $data['alias'];
        if(empty($alias)){
            $this->sendFail('PARAMETER_ERROR', 'alias参数错误');
        }
        $serviceModel = $this->get(SERVICE_MODEL);
        $model = $serviceModel->getModelByAlias($alias);
        if(empty($model)){
            $this->sendFail('PARAMETER_ERROR', '模型不存在');
        }
        $fields = $serviceModel->getFieldListByModel($model);
        if(empty($fields)){
            $this->sendFail('PARAMETER_ERROR', '模型没有字段');
        }
        return [$alias, $serviceModel, $model, $fields];
    }

    public function indexAction()
    {
        $data = $this->getRequestData();
        $search = $data;
        unset($search['alias'], $search['page'], $search['limit']);
        list($alias, , $model, ) = $this->check($data);
        $serviceAutomate = $this->get(SERVICE_AUTOMATE);
        $serviceAutomate->setReference($model);
        $extension = [
            'sort' => $model->dry_sort_sql,
            'search' => $search
        ];
        $send = $serviceAutomate->getIndex($data['page'], $data['limit'], $extension);
        /*调用回调函数处理数据 start*/
        $serviceCallback = $this->get(SERVICE_CALLBACK);
        $send = tryCall($serviceCallback, $alias, 'AfterIndex', $send, $serviceAutomate);
        /*调用回调函数处理数据 end*/
        $this->sendSuccess($send);
    }

    public function newAction()
    {
        $data = $this->getRequestData();
        $data = formArray2string($data);
        list($alias, , $model, $fields) = $this->check($data);
        unset($data['alias']);
        /*layui编辑器污染了表单*/
        unset($data['file']);
        /**/
        if(!empty($this->hasAddTimeField($fields))){
            $data = addDateTime($data, THE_TIME);
        }
        $serviceAutomate = $this->get(SERVICE_AUTOMATE);
        $serviceAutomate->setReference($model);
        $serviceCallback = $this->get(SERVICE_CALLBACK);
        $data = tryCall($serviceCallback, $alias, 'BeforeNew', $data, $serviceAutomate);
        $id = $serviceAutomate->add($data);
        tryCall($serviceCallback, $alias, 'AfterNew', $id, $serviceAutomate);
        /*如果(模型->配置)勾选了多级分类 start*/
        $config = explode(',', $model->dry_config);
        if(in_array('moreGrade', $config)){
            $this->get(SERVICE_CATEGORY)->setGrade($serviceAutomate, $id);
        }
        /*如果(模型->配置)勾选了多级分类 end*/
        $send = [
            'id' => $id
        ];
        $this->sendSuccess($send);
    }

    public function editAction()
    {
        $data = $this->getRequestData();
        $data = formArray2string($data);
        list($alias, , $model, ) = $this->check($data);
        $id = $data['id'];
        unset($data['id']);
        unset($data['alias']);
        /*layui编辑器污染了表单*/
        unset($data['file']);
        $serviceAutomate = $this->get(SERVICE_AUTOMATE);
        $serviceAutomate->setReference($model);
        /*调用回调函数处理数据 start*/
        $serviceCallback = $this->get(SERVICE_CALLBACK);
        $data = tryCall($serviceCallback, $alias, 'BeforeEdit', $data, $serviceAutomate);
        /*调用回调函数处理数据 end*/
        $rows = $serviceAutomate->update($data, $id);
        /*调用回调函数处理数据 start*/
        tryCall($serviceCallback, $alias, 'AfterEdit', $id, $serviceAutomate);
        /*调用回调函数处理数据 end*/
        /*如果(模型->配置)勾选了多级分类 start*/
        $config = explode(',', $model->dry_config);
        if(in_array('moreGrade', $config)){
            $this->get(SERVICE_CATEGORY)->setGrade($serviceAutomate, $id);
        }
        /*如果(模型->配置)勾选了多级分类 end*/
        $send = [
            'rows' => $rows
        ];
        $this->sendSuccess($send);
    }

    public function editPlusAction()
    {
        $data = $this->getRequestData();
        list(, , $model, ) = $this->check($data);
        $id = (int)$data['id'];
        $field = $data['field'];
        $value = $data['value'];
        $update = [
            $field => $value
        ];
        $serviceAutomate = $this->get(SERVICE_AUTOMATE);
        $serviceAutomate->setReference($model);
        $rows = $serviceAutomate->update($update, $id);
        $send = [
            'rows' => $rows
        ];
        $this->sendSuccess($send);
    }

    public function getListForChoiceAction()
    {
        $data = $this->getRequestData();
        list(, , $model, ) = $this->check($data);
        $fieldId = $data['fieldId'];
        unset($data['fieldId']);
        unset($data['alias']);
        $serviceAutomate = $this->get(SERVICE_AUTOMATE);
        $serviceAutomate->setConfigTable($model->dry_database_config_key, $model->dry_table);
        $serviceField = $this->get(SERVICE_FIELD);
        $field = $serviceField->one($fieldId);
        $list = $serviceAutomate->getListForChoice($field->dry_key_field, $field->dry_value_field, $field->dry_sql);
        if($field->dry_callback != ''){
            $list = $this->{$field->dry_callback}($list);
        }
        $result = [];
        foreach($list as $item){
            $result[] = ['key' => $item->k, 'text' => $item->v, 'class' => ''];
        }
        $send = [
            'list' => $result
        ];
        $this->sendSuccess($send);
    }

    public function getListForChoiceFromDataPoolAction()
    {
        $data = $this->getRequestData();
        $name = $data['name'];
        $serviceAutomate = $this->get(SERVICE_AUTOMATE);
        $serviceDataPool = $serviceAutomate->setAlias('data_pool');
        $dataPool = $serviceDataPool->getOneByField('dry_name', $name);
        $serviceAutomate = $serviceAutomate->setAlias($dataPool->dry_model_alias);
        $list = $serviceAutomate->getListForChoice($dataPool->dry_key_field, $dataPool->dry_value_field, $dataPool->dry_sql);
        if($dataPool->dry_callback != ''){
            $list = $this->{$dataPool->dry_callback}($list);
        }
        $result = [];
        foreach($list as $item){
            $result[] = ['key' => $item->k, 'text' => $item->v, 'class' => ''];
        }
        $send = [
            'list' => $result
        ];
        $this->sendSuccess($send);
    }

    /*
        根据分类模型别名取出所有数据，主要用在数据不多的情况
        /Api/Automate/getCategoryList?alias=mall_category
    */
    public function getCategoryListAction()
    {
        $data = $this->getRequestData();
        $alias = $data['alias'];
        $serviceAutomate = $this->get(SERVICE_AUTOMATE);
        $serviceAutomate = $serviceAutomate->setAlias($alias);
        $data = $serviceAutomate->getCategoryList();
        $this->sendSuccess($data);
    }

    private function defaultCallback($list)
    {
        foreach($list as $k => $item){
            $item->v = $item->id . '-' . $item->v;
            $list[$k] = $item;
        }
        return $list;
    }

    private function choiceCallback($list)
    {
        /*
        $object = object();
        $object->k = 0;
        $object->v = '作为顶级';
        array_unshift($list, $object);
        */
        return $list;
    }

    private function categoryCallback($list)
    {
        foreach($list as $k => $item){
            $item->v = getSpace($item->dry_grade) . $item->v;
            $list[$k] = $item;
        }
        return $list;
    }

}