<?php
namespace Application\Api\Controller;

class Model extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_MODEL);
    }

    public function indexAction()
    {
        $data = $this->getRequestData();
        $send = $this->getService()->getIndex($data['page'], $data['limit']);
        $this->sendSuccess($send);
    }

    public function newAction()
    {
        $service = $this->getService();
        $serviceModelField = $this->get(SERVICE_MODELFIELD);
        $data = $this->getRequestData();
        $data = formArray2string($data);
        $data = addDateTime($data, THE_TIME);
        $id = $service->add($data);
        /*字段明细 start*/
        $fieldList = explode(',', $data['dry_field']);
        foreach($fieldList as $k => $field){
            $insert = [
                'dry_model' => $id,
                'dry_field' => $field,
                'dry_sort' => $k + 1,
                'dry_field_list_sort' => $k + 1,
                'dry_field_search_sort' => $k + 1,
                'dry_field_search_default' => '',
                'dry_add_edit_is_show' => 1,
                'dry_is_init_hide' => 0,
                'dry_is_list_edit' => 0,
                'dry_is_list_click_sort' => 1,
                'dry_is_search' => 0,
                'dry_is_encrypt' => 0,
                'dry_sql_operate' => 'equal',
                'dry_field_template' => THE_NONE,
                'dry_field_width' => THE_NONE,
                'dry_first_choice' => THE_NONE,
                'dry_search_first_choice' => 'all'
            ];
            $insert = addDateTime($insert, THE_TIME);
            $serviceModelField->add($insert);
        }
        /*字段明细 end*/
        $send = [
            'id' => $id
        ];
        $this->sendSuccess($send);
    }

    public function editAction()
    {
        $service = $this->getService();
        $serviceModelField = $this->get(SERVICE_MODELFIELD);
        $data = $this->getRequestData();
        $data = formArray2string($data);
        $id = $data['id'];
        unset($data['id']);
        $rows = $service->update($data, $id);
        /*字段明细 start*/
        /*不需要的字段删除*/
        $serviceModelField->deleteByExclude($id, $data['dry_field']);
        /*已经有的字段*/
        $list = $serviceModelField->getListByModelId($id);
        $map = array2mapObject($list, 'dry_field');
        $fieldList = explode(',', $data['dry_field']);
        foreach($fieldList as $k => $field){
            if(isset($map[$field])){
                continue;
            }
            $insert = [
                'dry_model' => $id,
                'dry_field' => $field,
                'dry_sort' => $k + 1,
                'dry_field_list_sort' => $k + 1,
                'dry_field_search_sort' => $k + 1,
                'dry_field_search_default' => '',
                'dry_add_edit_is_show' => 1,
                'dry_is_init_hide' => 0,
                'dry_is_list_edit' => 0,
                'dry_is_list_click_sort' => 1,
                'dry_is_search' => 0,
                'dry_is_encrypt' => 0,
                'dry_sql_operate' => 'equal',
                'dry_field_template' => THE_NONE,
                'dry_field_width' => THE_NONE,
                'dry_first_choice' => THE_NONE,
                'dry_search_first_choice' => 'all'
            ];
            $insert = addDateTime($insert, THE_TIME);
            $serviceModelField->add($insert);
        }
        /*字段明细 end*/
        $send = [
            'rows' => $rows
        ];
        $this->sendSuccess($send);
    }

    public function getListAction()
    {
        $service = $this->getService();
        $list = $service->getList();
        $listNew = [];
        foreach($list as $v){
            $listNew[] = [
                'key' => $v->id,
                'text' => $v->dry_name,
                'class' => 'layui-bg-gray'
            ];
        }
        $send = [
            'list' => $listNew
        ];
        $this->sendSuccess($send);
    }

    public function getAliasListAction()
    {
        $service = $this->getService();
        $list = $service->getList();
        $listNew = [];
        $listNew[] = [
            'key' => 'none',
            'text' => '无',
            'class' => ''
        ];
        foreach($list as $v){
            $listNew[] = [
                'key' => $v->dry_alias,
                'text' => $v->dry_name,
                'class' => ''
            ];
        }
        $send = [
            'list' => $listNew
        ];
        $this->sendSuccess($send);
    }

}