<?php
namespace Application\Admin\Controller;

class FieldType extends Base
{

    public function init()
    {
        $this->setDir('field_type');
    }

    public function _initRoute()
    {
        $data = [
            'path_new' => '/Admin/FieldType/new',
            'path_edit' => '/Admin/FieldType/edit',
            'path_index' => '/Admin/FieldType/index',
            'path_show' => '/Admin/FieldType/show',
            'path_delete' => '/Admin/FieldType/delete'
        ];
        $this->setCommonData($data);
    }

    private function getService()
    {
        return $this->get(SERVICE_FIELDTYPE);
    }

    public function indexAction()
    {
        $this->setFile($this->getTemplateIndex());
        $this->show();
    }

    public function newAction()
    {
        $this->setFile($this->getTemplateNew());
        $this->setData(['action' => 'new']);
        $this->show();
    }

    public function editAction()
    {
        $data = $this->getRequestData();
        $rs = $this->getService()->one($data['id']);
        $this->setFile($this->getTemplateEdit());
        $this->setData(['action' => 'edit', 'rs' => $rs]);
        $this->show();
    }

    public function showAction()
    {
        $data = $this->getRequestData();
        $rs = $this->getService()->one($data['id']);
        $this->setFile($this->getTemplateShow());
        $this->setData(['action' => 'show', 'rs' => $rs]);
        $this->show();
    }

}