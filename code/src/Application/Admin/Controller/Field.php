<?php
namespace Application\Admin\Controller;

class Field extends Base
{

    public function init()
    {
        $this->setDir('field');
    }

    public function _initRoute()
    {
        $data = [
            'path_new' => '/Admin/Field/new',
            'path_edit' => '/Admin/Field/edit',
            'path_index' => '/Admin/Field/index',
            'path_show' => '/Admin/Field/show',
            'path_delete' => '/Admin/Field/delete'
        ];
        $this->setCommonData($data);
    }

    private function getService()
    {
        return $this->get(SERVICE_FIELD);
    }

    public function indexAction()
    {
        $this->setFile($this->getTemplateIndex());
        $this->show();
    }

    public function newAction()
    {
        $this->setFile($this->getTemplateNew());
        $this->setData(['action' => 'new']);
        $this->show();
    }

    public function editAction()
    {
        $data = $this->getRequestData();
        $rs = $this->getService()->one($data['id']);
        $this->setFile($this->getTemplateEdit());
        $this->setData(['action' => 'edit', 'rs' => $rs]);
        $this->show();
    }

    public function showAction()
    {
        $data = $this->getRequestData();
        $rs = $this->getService()->one($data['id']);
        $this->setFile($this->getTemplateShow());
        $this->setData(['action' => 'show', 'rs' => $rs]);
        $this->show();
    }

}