<?php
namespace Crontab;

class Make extends Base
{

    /*
        /usr/bin/php -d display_error /app/cli.php local Make.service
    */
    public function service()
    {
        $applicationConfig = $this->getInstance('applicationConfig');
        $content1 = [];
        $content2 = [];
        $content1[] = '<?php';
        $content2[] = 'services:';
        foreach($applicationConfig['service']['list'] as $dir){
            $list = readFileFromDir($dir);
            foreach($list as $file){
                if(!in_array($file, $applicationConfig['service']['filter'])){
                    $temp = str_replace(SRC_PATH, '', $file);
                    $temp = str_replace('.php', '', $temp);
                    $temp = trim($temp, '/');
                    $temp = explode('/', $temp);
                    $information = parseService($temp);
                    $content1[] = "define('{$information['upper']}', '{$information['lower']}');";
                    $content2[] = "    {$information['lower']}:";
                    $content2[] = "        class: {$information['service']}";
                    $content2[] = "        single: 0";
                }
            }
        }
        $content1 = implode(PHP_EOL, $content1);
        $content2 = implode(PHP_EOL, $content2);
        file_put_contents(CONSTANT_PATH . '/service.php', $content1);
        file_put_contents(CONFIG_PATH . '/service.yaml', $content2);
    }

    /*
        /usr/bin/php -d display_error /app/cli.php local Make.signPass2file
    */
    public function signPass2file()
    {
        $serviceChoice = $this->get(SERVICE_CHOICE);
        $list = $serviceChoice->getListByKey('signPass');
        $data = [];
        $data[] = 'default:';
        foreach($list as $item){
            $data[] = '    - ' . $item->dry_key;
        }
        $file = CONFIG_PATH . '/common/signPass.yaml';
        file_put_contents($file, implode(PHP_EOL, $data));
    }

}