<?php
$error = "/usr/bin/php -d display_error /app/cli.php local|product class.method ['json']" . PHP_EOL;
if($argc < 3){
    exit($error);
}
if(!in_array($argv[1], ['local', 'product'])){
    exit($error);
}
ini_set('memory_limit', '512M');
define('PROJECT_PATH', dirname(__FILE__));
require_once(PROJECT_PATH . '/src/Php/init.php');
$classMethod = explode('.', $argv[2]);
if(count($classMethod) != 2){
    exit($error);
}
$parameter = [];
if(isset($argv[3])){
    $parameter = json_decode($argv[3], true);
    if(!$parameter){
        exit($error);
    }
}
$class = $classMethod[0];
$method = $classMethod[1];
$classFull = "Crontab\\{$class}";
if(!class_exists($classFull)){
    exit(CLASS_ERROR);
}
$instance = new $classFull();
if(!method_exists($instance, $method)){
    exit(METHOD_ERROR);
}
$instance->{$method}($parameter);