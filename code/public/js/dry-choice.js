var statusData = [
    {"key":"0","text":"禁用","class":"layui-bg-red"},
    {"key":"1","text":"启用","class":"layui-bg-green"}
];
var sexData = [
    {"key":"1","text":"男","class":""},
    {"key":"2","text":"女","class":""},
    {"key":"3","text":"保密","class":""}
];
var languageGroupData = [
    {"key":"common","text":"通用","class":""},
    {"key":"extension","text":"拓展","class":""},
    {"key":"model","text":"模型","class":""}
];
var yesNoData = [
    {"key":"0","text":"否","class":""},
    {"key":"1","text":"是","class":"layui-bg-green"}
];
var fieldDataSourceData = [
    {"key":"0","text":"无","class":""},
    {"key":"1","text":"变量","class":""},
    {"key":"2","text":"接口(不推荐)","class":""},
    {"key":"3","text":"模型(不推荐)","class":""},
    {"key":"4","text":"数据池","class":""}
];
var formTypeData = [
    {"key":"none","text":"无","class":""},
    {"key":"id","text":"ID","class":""},
    {"key":"input","text":"输入框","class":""},
    {"key":"password","text":"密码框","class":""},
    {"key":"textarea","text":"文本域","class":""},
    {"key":"date","text":"日期选择","class":""},
    {"key":"time","text":"时间选择","class":""},
    {"key":"datetime","text":"日期时间选择","class":""},
    {"key":"radio","text":"单选","class":""},
    {"key":"checkbox","text":"多选","class":""},
    {"key":"checkboxBig","text":"多选大号","class":""},
    {"key":"select","text":"下拉单选","class":""},
    {"key":"transfer","text":"穿梭机","class":""},
    {"key":"switch","text":"开关","class":""},
    {"key":"fileSelect","text":"文件选择","class":""},
    {"key":"color","text":"颜色选择","class":""},
    {"key":"editor","text":"富文本编辑器","class":""},
    {"key":"icon","text":"ICON选择","class":""},
    {"key":"buttonTheme","text":"按钮主题","class":""}
];
var variableData = [
    {"key":"yesNoData","text":"是否","class":""},
    {"key":"statusData","text":"状态","class":""}
];
var domainData = [
    {"key":"*","text":"自动判断","class":""}
];
var fieldWidthData = [
    {"key":"none","text":"无","class":""},
    {"key":"ID_WIDTH","text":"ID","class":""},
    {"key":"SORT_WIDTH","text":"排序","class":""},
    {"key":"STATUS_WIDTH","text":"状态","class":""},
    {"key":"ADD_TIME_WIDTH","text":"添加时间","class":""},
    {"key":"OPERATE_WIDTH","text":"操作","class":""},
    {"key":"DATA_SORT_WIDTH","text":"数据排序","class":""},
    {"key":"ICON_WIDTH","text":"ICON","class":""},
    {"key":"GRADE_WIDTH","text":"等级","class":""},
    {"key":"LINK_WIDTH","text":"链接","class":""},
    {"key":"PHONE_WIDTH","text":"手机","class":""},
    {"key":"FILE_POSTER_WIDTH","text":"文件封面","class":""},
    {"key":"COLOR_WIDTH","text":"颜色","class":""},
    {"key":"ORDER_NO_WIDTH","text":"订单号","class":""},
    {"key":"WIDTH_100","text":"100","class":""},
    {"key":"WIDTH_110","text":"110","class":""},
    {"key":"WIDTH_120","text":"120","class":""},
    {"key":"WIDTH_130","text":"130","class":""},
    {"key":"WIDTH_140","text":"140","class":""},
    {"key":"WIDTH_150","text":"150","class":""},
    {"key":"WIDTH_160","text":"160","class":""},
    {"key":"WIDTH_170","text":"170","class":""},
    {"key":"WIDTH_180","text":"180","class":""},
    {"key":"WIDTH_190","text":"190","class":""},
    {"key":"WIDTH_200","text":"200","class":""},
    {"key":"WIDTH_210","text":"210","class":""},
    {"key":"WIDTH_220","text":"220","class":""},
    {"key":"WIDTH_230","text":"230","class":""},
    {"key":"WIDTH_240","text":"240","class":""},
    {"key":"WIDTH_250","text":"250","class":""}
];
var additionalColumnData = [
    {"key":"none","text":"无","class":""},
    {"key":"radio","text":"单选","class":""},
    {"key":"checkbox","text":"多选","class":""}
];
var pageSizeData = [
    {"key":"10","text":"10","class":""},
    {"key":"50","text":"50","class":""},
    {"key":"100","text":"100","class":""},
    {"key":"200","text":"200","class":""}
];
var fieldTemplateData = [
    {"key":"none","text":"无","class":""},
    {"key":"htmlShow","text":"HTML","class":""},
    {"key":"mergeRequestTag","text":"合并请求-标签","class":""},
    {"key":"mergeRequestFile","text":"合并请求-文件","class":""},
    {"key":"iconShow","text":"ICON","class":""},
    {"key":"linkShow","text":"链接","class":""},
    {"key":"indentShow","text":"缩进","class":""},
    {"key":"colorShow","text":"颜色","class":""},
    {"key":"buttonThemeShow","text":"按钮主题","class":""},
    {"key":"codeShow","text":"CODE","class":""},
    {"key":"jsonShow","text":"JSON","class":""}
];
var buttonPositionData = [
    {"key":"tableLeftTop","text":"表格左上角","class":""},
    {"key":"cardRightTop","text":"面板右上角","class":""},
    {"key":"dataRow","text":"数据行","class":""}
];
var modelConfigData = [
    {"key":"none","text":"无","class":""},
    {"key":"new","text":"新窗口打开","class":""},
    {"key":"moreGrade","text":"多级分类","class":""}
];
var dataPoolDataSourceData = [
    {"key":"1","text":"接口","class":""},
    {"key":"2","text":"模型","class":""}
];
var apiData = [
    {"key":"none","text":"无","class":""},
    {"key":"\/Api\/Menu\/getList","text":"[接口]菜单列表","class":""},
    {"key":"\/Api\/Automate\/getListForChoiceFromDataPool","text":"[数据池]根据名称获取数据","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=status","text":"[选项]状态","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=languageGroup","text":"[选项]语言分组","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=yesNo","text":"[选项]是否","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=buttonPosition","text":"[选项]按钮位置","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=dataPoolDataSource","text":"[选项]数据池数据源","class":""},
    {"key":"\/Api\/Automate\/getListForChoice","text":"[表]动态表数据","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=domain","text":"[选项]域名","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=api","text":"[选项]API地址","class":""},
    {"key":"\/Api\/Model\/getAliasList","text":"[接口]模型别名列表","class":""},
    {"key":"\/Api\/File\/getListByIds","text":"[文件]文件查询接口","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=productRecommend","text":"[选项]产品推荐","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=downloadCategory","text":"[选项]下载分类","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=sqlOperate","text":"[选项]sql操作符","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=firstChoice","text":"[选项]数据源的首个选项","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=payType","text":"[选项]支付方式","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=constantGroup","text":"[选项]常量分组","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=orderStatus","text":"[选项]订单状态","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=mainProduct","text":"[选项]主产品","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=mainProductSpec","text":"[选项]主产品规格","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=withdrawType","text":"[选项]提现方式","class":""},
    {"key":"\/Api\/Choice\/getListByKey?key=withdrawStatus","text":"[选项]提现状态","class":""}
];
var productRecommendData = [
    {"key":"home","text":"首页","class":""}
];
var downloadCategoryData = [
    {"key":"golang","text":"go语言程序","class":""}
];
var sqlOperateData = [
    {"key":"equal","text":"等于","class":""},
    {"key":"not_equal","text":"不等于","class":""},
    {"key":"lt","text":"小于","class":""},
    {"key":"lte","text":"小于等于","class":""},
    {"key":"gt","text":"大于","class":""},
    {"key":"gte","text":"大于等于","class":""},
    {"key":"left_like_right","text":"包含","class":""},
    {"key":"left_not_like_right","text":"不包含","class":""},
    {"key":"like_right","text":"开始以","class":""},
    {"key":"not_like_right","text":"开始不是以","class":""},
    {"key":"left_like","text":"结束以","class":""},
    {"key":"left_not_like","text":"结束不是以","class":""},
    {"key":"is_null","text":"是null","class":""},
    {"key":"is_not_null","text":"不是null","class":""},
    {"key":"is_empty","text":"是空的","class":""},
    {"key":"is_not_empty","text":"不是空的","class":""},
    {"key":"between_and","text":"介于","class":""},
    {"key":"not_between_and","text":"不介于","class":""},
    {"key":"in","text":"在列表","class":""},
    {"key":"not_in","text":"不在列表","class":""},
    {"key":"contain","text":"存在某项","class":""},
    {"key":"not_contain","text":"不存在某项","class":""},
    {"key":"match_word","text":"包含词","class":""}
];
var firstChoiceData = [
    {"key":"none","text":"无","class":""},
    {"key":"top","text":"作为顶级","class":""},
    {"key":"all","text":"全部","class":""},
    {"key":"select","text":"请选择","class":""}
];
var signPassData = [
    {"key":"\/Api\/Choice\/getListByKey","text":"选项获取数据","class":""},
    {"key":"\/Api\/DataPool\/getData","text":"数据池获取数据","class":""},
    {"key":"\/Api\/User\/isLogin","text":"检查用户是否登录","class":""}
];
var payTypeData = [
    {"key":"1","text":"支付宝","class":""},
    {"key":"2","text":"微信","class":""},
    {"key":"3","text":"微信小程序","class":""}
];
var orderStatusData = [
    {"key":"1","text":"待确认","class":""},
    {"key":"2","text":"已支付","class":""},
    {"key":"3","text":"已取消","class":""},
    {"key":"4","text":"已完成","class":""}
];
var constantGroupData = [
    {"key":"orderStatus","text":"订单状态","class":""},
    {"key":"common","text":"通用","class":""},
    {"key":"payType","text":"支付方式","class":""},
    {"key":"width","text":"宽度","class":""},
    {"key":"device","text":"设备","class":""},
    {"key":"accountLogType","text":"金额日志类型","class":""},
    {"key":"withdrawType","text":"提现方式","class":""},
    {"key":"withdrawStatus","text":"提现状态","class":""}
];
var mainProductData = [
    {"key":"drycms","text":"DRYCMS软件","class":""}
];
var mainProductSpecData = [
    {"key":"agent_t","text":"铜牌代理","class":""},
    {"key":"agent_y","text":"银牌代理","class":""},
    {"key":"agent_j","text":"金牌代理","class":""},
    {"key":"a","text":"A套餐","class":""},
    {"key":"b","text":"B套餐","class":""},
    {"key":"c","text":"C套餐","class":""}
];
var withdrawTypeData = [
    {"key":"1","text":"支付宝","class":""},
    {"key":"2","text":"微信","class":""},
    {"key":"3","text":"银行卡","class":""}
];
var withdrawStatusData = [
    {"key":"1","text":"待审核","class":""},
    {"key":"2","text":"已通过","class":""},
    {"key":"3","text":"已拒绝","class":""},
    {"key":"4","text":"已打款","class":""},
    {"key":"5","text":"已完成","class":""}
];